/* ["stroke","strokeWidth","fill","fontFamily","fontSize","fontWeight","fontStyle","underline","overline","linethrough","deltaY","textBackgroundColor"] */

fabric.Object.prototype.transparentCorners = false;
fabric.Object.prototype.padding = 5;


var $ = function(id) {
    return document.getElementById(id)
};


var canvas = this.__canvas = new fabric.Canvas('c');
canvas.setHeight(300);
canvas.setWidth(500);


function AddImage(imageURL = 'http://fabricjs.com/assets/pug_small.jpg') {
    fabric.Image.fromURL(imageURL, function(myImg) {
        var image = myImg.set({
            left: 0,
            top: 0,
            // width: 150,
            // height: 150,
            angle: 0,
            padding: 10,
            cornersize: 10,
            hasRotatingPoint: true
        });
        image.scaleToHeight(100);
        // image.scaleToWidth(200);
        canvas.add(image);
    });
}

function Addtext(text = 'Utkarsh') {
    canvas.add(new fabric.IText(text, {
        left: 50,
        top: 100,
        fontFamily: 'arial black',
        fill: '#333',
        fontSize: 50
    }));
}

document.getElementById('text-color').onchange = function() {
    canvas.getActiveObject().set('color', this.value);
    canvas.renderAll();
};

document.getElementById('text-bg-color').onchange = function() {
    canvas.getActiveObject().set('backgroundColor', this.value);
    canvas.renderAll();
};

document.getElementById('text-lines-bg-color').onchange = function() {
    canvas.getActiveObject().set('textBackgroundColor', this.value);
    canvas.renderAll();
};

document.getElementById('text-stroke-color').onchange = function() {
    canvas.getActiveObject().setStroke(this.value);
    canvas.renderAll();
};

document.getElementById('text-stroke-width').onchange = function() {
    canvas.getActiveObject().setStrokeWidth(this.value);
    canvas.renderAll();
};

document.getElementById('font-family').onchange = function() {
    canvas.getActiveObject().setFontFamily(this.value);
    canvas.renderAll();
};

document.getElementById('text-font-size').onchange = function() {
    canvas.getActiveObject().setFontSize(this.value);
    canvas.renderAll();
};

document.getElementById('text-line-height').onchange = function() {
    canvas.getActiveObject().setLineHeight(this.value);
    canvas.renderAll();
};

document.getElementById('text-align').onchange = function() {
    canvas.getActiveObject().setTextAlign(this.value);
    canvas.renderAll();
};
document.getElementById('text-cmd-underline').onchange = function() {
    canvas.getActiveObject().set("underline", this.checked);
    canvas.renderAll();
};


radios5 = document.getElementsByName("fonttype"); // wijzig naar button
for (var i = 0, max = radios5.length; i < max; i++) {
    radios5[i].onclick = function() {

        if (document.getElementById(this.id).checked == true) {
            if (this.id == "text-cmd-bold") {
                console.log(canvas.getActiveObject());
                canvas.getActiveObject().set("fontWeight", "bold");
            }
            if (this.id == "text-cmd-italic") {
                canvas.getActiveObject().set("fontStyle", "italic");
            }
            if (this.id == "text-cmd-linethrough") {
                canvas.getActiveObject().set("textDecoration", "line-through");
            }
            if (this.id == "text-cmd-overline") {
                canvas.getActiveObject().set("textDecoration", "overline");
            }



        } else {
            if (this.id == "text-cmd-bold") {
                canvas.getActiveObject().set("fontWeight", "");
            }
            if (this.id == "text-cmd-italic") {
                canvas.getActiveObject().set("fontStyle", "");
            }
            if (this.id == "text-cmd-underline") {
                canvas.getActiveObject().set("textDecoration", "");
            }
            if (this.id == "text-cmd-linethrough") {
                canvas.getActiveObject().set("textDecoration", "");
            }
            if (this.id == "text-cmd-overline") {
                canvas.getActiveObject().set("textDecoration", "");
            }
        }


        canvas.renderAll();
    }
}
